﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Counties.Models;
using Microsoft.EntityFrameworkCore;

namespace Counties.Controllers
{
	public class HomeController : Controller
	{
		#region
		//public IActionResult Index()
		//{
		//    return View();
		//}

		//public IActionResult About()
		//{
		//    ViewData["Message"] = "Your application description page.";

		//    return View();
		//}

		//public IActionResult Contact()
		//{
		//    ViewData["Message"] = "Your contact page.";

		//    return View();
		//}

		//public IActionResult Error()
		//{
		//    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		//}
		#endregion

		private RegionContext db;
		public HomeController(RegionContext context)
		{
			db = context;
		}

		public IActionResult Index()
		{
			return View(RedirectToAction("Index"));
		}

		public IActionResult Create()
		{
			return View(); 
		}

		[HttpPost]
		public async Task<IActionResult> Create(Region region)
		{
			db.Regions.Add(region);
			await db.SaveChangesAsync();
			return RedirectToAction("Index");
		}

		public IActionResult SampleTable()
		{
			return View(RedirectToAction("SampleTable"));
		}

		public async Task<IActionResult> SamplePrintDB()
		{
			return View(await db.Regions.ToListAsync());
		}

		public async Task<IActionResult> Details(int? id)
		{
			if (id != null)
			{
				Region region = await db.Regions.FirstOrDefaultAsync(p => p.Id == id);
				if (region != null)
					return View(region);
			}
			return NotFound();
		}

		public async Task<IActionResult> Edit(int? id)
		{
			if (id != null)
			{
				Region region = await db.Regions.FirstOrDefaultAsync(p => p.Id == id);
				if (region != null)
					return View(region);
			}
			return NotFound();
		}
		[HttpPost]
		public async Task<IActionResult> Edit(Region region)
		{
			db.Regions.Update(region);
			await db.SaveChangesAsync();
			return RedirectToAction("Index");
		}

		[HttpGet]
		[ActionName("Delete")]
		public async Task<IActionResult> ConfirmDelete(int? id)
		{
			if (id != null)
			{
				Region region = await db.Regions.FirstOrDefaultAsync(p => p.Id == id);
				if (region != null)
					return View(region);
			}
			return NotFound();
		}

		[HttpPost]
		public async Task<IActionResult> Delete(int? id)
		{
			if (id != null)
			{
				Region region = await db.Regions.FirstOrDefaultAsync(p => p.Id == id);
				if (region != null)
				{
					db.Regions.Remove(region);
					await db.SaveChangesAsync();
					return RedirectToAction("Index");
				}
			}
			return NotFound();
		}
	}
}
