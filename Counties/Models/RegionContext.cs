﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Counties.Models
{
	public class RegionContext : DbContext
	{
		public DbSet<Region> Regions { get; set; }
		public RegionContext(DbContextOptions<RegionContext> options)
			: base(options)
		{
			Database.EnsureCreated();
		}
	}
}
