﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Counties
{
	/// <summary>
	/// Класс хранит всю информацию для данной страны
	/// </summary>
	public class Region
	{

		public int Id { get; set; } // key of field in DB

		#region
		/// Main information
		public string Name { get; set; }
		public string PartyToTheConvention { get; set; }
		public string ComponentAithority { get; set; }
		public string FocalPoint { get; set; }
		public string ContactPointsToLANSystem { get; set; }
		public string ParticipationInCOPs { get; set; }
		public string MemberOfSubsidiary { get; set; }
		public string Body { get; set; }
		public string FactFinding_AwarenessRaisingMissions { get; set; }
		public string ImplementationReports { get; set; }
		public string SelfAssessment { get; set; }
		public string ActionPlan { get; set; }

		public string AssistanceProgrammeActivities { get; set; }
		public string OtherCapacityBuildingActivities { get; set; }
		public string ConclusionsAndRecommendations { get; set; }
		public string UpcomingActivities { get; set; }
		public string SelfAssessment_ActionPlan { get; set; }
		public string Comments_Analysis { get; set; }
		public string ContactsWithCountry_ConclusionsFromTele { get; set; }
		public string EngagementStrategy { get; set; }
		public string FollowUp { get; set; }
		#endregion
	}
}
